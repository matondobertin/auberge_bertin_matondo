<?php
$connect = mysqli_connect("127.0.0.1", "root","","auberge" );

if($connect){
    if(isset($_POST["search"])){
        $search = trim(htmlspecialchars($_POST["search"]));
        $sql = "SELECT * FROM chambre WHERE nom LIKE '$search%'";
        $res = mysqli_prepare($connect,$sql);
        $sql_exe = mysqli_stmt_execute($res);
        mysqli_stmt_bind_result($res, $num_ch, $prix, $nb_lit, $nb_pers,$confort,$img,$descr);

    }else{
        $sql = "SELECT * FROM chambre";
        $res = mysqli_prepare($connect,$sql);
        $sql_exe = mysqli_stmt_execute($res);
        mysqli_stmt_bind_result($res, $num_ch, $prix, $nb_lit, $nb_pers,$confort,$img,$descr,$categorie,$etat);

    }     
}

require_once("partials/header.php");
?>

<div class="col-xs-8 col-xs-offset-2 mt-3">
    <form action="" method="post">
        <div class="input-group">
            <div class="input-group-btn search-panel">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span id="search_concept">Filter by</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li class="ml-2"><a href="#contains">Prix croissants</a></li>
                    <li class="ml-2"><a href="#its_equal">Prix décroissants</a></li>
                </ul>
            </div>      
                <input type="text" class="form-control" name="search" placeholder="Search term...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i><span class="glyphicon glyphicon-search"></span></button>
                </span>
        </div>
    </form>
</div>


<div class="col-auto listing-block">
    <div class="row">

    <?php if($res){
    while($tab_ch = mysqli_stmt_fetch($res)){?>

        <div class="m-3 media col border d-flex">
              <img class="d-flex" width="250px"src="images/<?=$img;?>" alt="chambre">
              <div class="media-body pl-3">
                <div class="price"><?=$prix;?> &euro;<small> Chambre n° : <?=$num_ch;?> </small></div>
                <div class="stats">
                    <span><i class="fa fa-arrows-alt"></i> <?=$categorie?></span>
                    <span><i class="fa fa-bed"></i> <?=$nb_lit;?> Lits</span>
                </div><br>
                <div class="address text-justify">Equipement :<br><small><?=$confort;?></small></div>
                <div class="text-right m-3">
                    <a href="details.php?num_ch=<?=$num_ch?>" class="btn btn-warning"><i class ="fa fa-info-circle"></i> Informations</a>
                </div>
              </div>
        </div>
        
    <?php }}?>

</div>
</div>
<?php require_once("partials/footer.php");?>