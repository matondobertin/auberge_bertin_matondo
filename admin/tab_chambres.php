<?php
$connect = mysqli_connect("127.0.0.1", "root","","auberge" );

if($connect){
    if(isset($_POST["search"])){
        $search = trim(htmlspecialchars($_POST["search"]));
        $sql = "SELECT * FROM chambre WHERE nom LIKE '$search%'";
        $res = mysqli_prepare($connect,$sql);
        $sql_exe = mysqli_stmt_execute($res);
        mysqli_stmt_bind_result($res, $num_ch, $prix, $nb_lit, $nb_pers,$confort,$img,$descr,$categorie,$etat);

    }else{
        $sql = "SELECT * FROM chambre";
        $res = mysqli_prepare($connect,$sql);
        $sql_exe = mysqli_stmt_execute($res);
        mysqli_stmt_bind_result($res, $num_ch, $prix, $nb_lit, $nb_pers,$confort,$img,$descr,$categorie,$etat);

    }     
}

require_once("../partials/header.php");
?>

<div class="col-xs-8 col-xs-offset-2 mt-3">
<h1 class="text-center">Listes Des Chambres du VALENTINE'S</h1>
    <form action="" method="post">
        <div class="input-group">
            <div class="input-group-btn search-panel">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span id="search_concept">Filter by</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li class="ml-2"><a href="#contains">Prix croissants</a></li>
                    <li class="ml-2"><a href="#its_equal">Prix décroissants</a></li>
                </ul>
            </div>      
                <input type="text" class="form-control" name="search" placeholder="Search term...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i><span class="glyphicon glyphicon-search"></span></button>
                </span>
        </div>
    </form>
</div>

<div class="text-right m-4">
    <a href="add.php" class="btn btn-warning"><i class ="fa fa-plus-circle"></i> Ajouter</a>
    </div>

<div class="col-auto listing-block">

 <table class="table table-bordered table-dark mt-2 ">
        <thead class="text-center">
            <tr>
            <th>Catégorie</th>
            <th>N° de chambre</th>
            <th>Prix</th>
            <th>Nombre de lits</th>
            <th>Capacité</th>
            <th>Image</th>
            <th>Description</th>
            <th>Etat</th>
            <th class="" colspan ="2">Actions</th>
            </tr>
        </thead>
        <tbody>
        <?php if($res){
            while($tab_ch = mysqli_stmt_fetch($res)){?>

            <tr>
                <td class="align-middle"><?=$categorie?></td>
                <td class="align-middle"><?=$num_ch?></td>
                <td class="align-middle"><?=$prix?></td>
                <td class="align-middle"><?=$nb_lit?></td>
                <td class="align-middle"><?=$nb_pers?></td>
                <td class="align-middle"><img src="../Images/<?=$img?>" alt ="user" width ="150"></td>
                <td class="align-middle"><?=$descr?></td>
                <td class="align-middle"><?=$etat?></td>


                <td class="align-middle" id ="update"><a class="btn btn-success" href="http://localhost/php/procedurale/auberge/admin/update.php?num_ch=<?=$num_ch;?>"><i class = "fa fa-pencil"></i> Editer</a></td>
                <td class="align-middle" id ="delete"><a onclick = "return confirm('Êtes vous sûr de vouloir supprimer ?')" class="btn btn-danger" href="delete.php?num_ch=<?=$num_ch;?>"><i class = "fa fa-trash"></i> Supprimer</a></td>            
            </tr>
                    <?php }}?>
        </tbody>
    </table>       
</div>

<?php require_once("../partials/footer.php");?>