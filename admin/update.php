<?php
require_once("../connexion.php");
if($connect){

    if(isset($_GET["num_ch"])){
        $num_ch = (int)trim(htmlspecialchars($_GET["num_ch"]));
        $sql_recap = "SELECT * FROM chambre WHERE num_chambre =".$num_ch;

        $res_recap = mysqli_query($connect,$sql_recap);
        if($res_recap){
            $recap = mysqli_fetch_assoc($res_recap);

        }
    }

    if(isset($_POST["submit"]) && !empty($_POST["categorie"]) && !empty($_POST["capacite"]) && !empty($_POST["nb_lit"]) && !empty($_POST["prix"]) && !empty($_POST["etat"]) && !empty($_POST["descr"])){
        $categorie = trim(addslashes(htmlentities($_POST["categorie"])));
        $pers = (int)trim(addslashes(htmlentities($_POST["capacite"])));
        $lit = (int)trim(addslashes(htmlentities($_POST["nb_lit"])));
        $prix = (int)trim(addslashes(htmlentities($_POST["prix"])));
        $etat = trim(addslashes(htmlentities($_POST["etat"])));
        $descr = trim(addslashes(htmlentities($_POST["descr"])));
        
        $img = $_FILES["img"]["name"];
        $dest = "Images/";
        move_uploaded_file($_FILES["img"]["tmp_name"], $dest.$_FILES["img"]["name"]);

        $sql = "UPDATE chambre SET num_chambre = ?, prix = ?, nb_lit = ?, nb_pers = ?, confort = ?, image = ?, description = ?, categorie =?, etat = ? WHERE num_chambre=".$num_ch;



        $res = mysqli_prepare($connect, $sql);

        mysqli_stmt_bind_param($res,"iiiisssss",$num_ch,$prix, $lit, $pers, $equipment, $img, $descr, $categorie, $etat);

        $exe = mysqli_stmt_execute($res);
        if($exe){
            header("location:tab_chambres.php");
        }else{
            echo 'Echec lors de l\'insertion';
        }
    }
}



?>

<?php require_once('../partials/header.php');?>

<h1>Chambre n° : <?=$num_ch;?></h1>
<form class="m-4" action="" method="post" enctype ="multipart/form-data">
  <div class="row">
    <div class="col">
    <label>Catégories</label>
    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="categorie">
        <option value=""><?=$recap["categorie"]?></option>
        <option value="Suites classiques">Suites classiques</option>
        <option value="Chambre simple">Chambre simple</option>
        <option value="Chambre double">Chambre double</option>
      </select>
    </div>
    <div class="col">
    <label>Capacité max.</label>
    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="capacite">
        <option ><?=$recap["nb_pers"]?></option>
        <option value="1">1 personne</option>
        <option value="2">2 personnes</option>
        <option value="3">3 personnes</option>
        <option value="4">4 personnes</option>
        <option value="5">5 personnes</option>
        <option value="6">6 personnes</option>
      </select>
    </div>
    <div class="col">
    <label>Nombre de lits</label>
    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="nb_lit">
        <option><?=$recap["nb_lit"]?></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
      </select>
    </div>
    <div class="col">
    <label>Prix</label>
      <input type="text" class="form-control" placeholder="Prix" name="prix" value="<?=$recap["prix"]?>">
    </div>
  </div>
  <div class="row">
    <div class="custom-file col-9 mt-5">
        <input type="file" class="custom-file-input" id="validatedCustomFile" name="img" required>
        <label class="custom-file-label" for="validatedCustomFile"><?=$recap["image"]?></label>
    </div>
    <div class="col-2 m-3 ">
    <label>Etat</label>
    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="etat">
        <option><?=$recap["etat"]?></option>
        <option value="Disponible">Disponible</option>
        <option value="Reservée">Réservée</option>
      </select>
    </div>
  </div>
<div class="row m-2">
<div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="confort[Wifi]">
        <label class="form-check-label">Wifi</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="confort[TV-LCD]">
        <label class="form-check-label">TV-LCD</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="confort[Coffre-fort]">
        <label class="form-check-label">Coffre-fort</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="confort[Cafétière]">
        <label class="form-check-label" for="inlineCheckbox1">Cafétière</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="confort[Snacks]">
        <label class="form-check-label" for="inlineCheckbox2">Snacks</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="confort[Baignoire]">
        <label class="form-check-label" for="inlineCheckbox1">Baignoire</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="confort[Jacuzzi]">
        <label class="form-check-label" for="inlineCheckbox1">Jacuzzi</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="confort[Double-douche]">
        <label class="form-check-label" for="inlineCheckbox2">Double-douche</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="confort[Non-fumeur]">
        <label class="form-check-label" for="inlineCheckbox2">Non-fumeur</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="confort[Climatisation]">
        <label class="form-check-label" for="inlineCheckbox2">Climatisation</label>
    </div>
</div>

<div class="row justify-content-center">
    <div class="form-group w-100 m-3">
    <textarea class="form-control" id="exampleFormControlTextarea3" rows="15" placeholder="Description..." name="descr"><?=$recap["description"]?></textarea>
    </div>
</div>

<div class="text-right m-4">
    <a href="http://localhost/php/procedurale/auberge/admin/tab_chambres.php" class="btn btn-danger" name="cancel" onclick = "return confirm('Êtes vous sûr de vouloir annuler vos modifications ?')" ><i class ="fa fa-times-circle"></i> Annuler</a>
    <button class="btn btn-success" type="submit" name="submit" onclick = "return confirm('Êtes vous sûr de vouloir appliquer vos modifications ?')" ><i class ="fa fa-pencil"></i> Modifier</button>

</div>

</form>

<?php require_once("../partials/footer.php");?>