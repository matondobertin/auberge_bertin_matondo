<?php
$connect = mysqli_connect("127.0.0.1", "root","","auberge" );
require_once("../partials/header.php");
?>

<?php

if(isset($_POST["submit"])){
  if(!empty($_POST["num_ch"]) && !empty($_POST["entrer"]) && !empty($_POST["sortir"]) && !empty($_POST["pers"])){
    $num_cl= (int)trim(addslashes(htmlentities($_POST["num_cl"])));
    $num_ch= (int)trim(addslashes(htmlentities($_POST["num_ch"])));
    $enter= trim(addslashes(htmlentities($_POST["entrer"])));
    $exit= trim(addslashes(htmlentities($_POST["sortir"])));
    $pers= (int)trim(addslashes(htmlentities($_POST["pers"])));


    if($connect){
        $sql = "INSERT INTO reservation (num_client, num_chambre, date_arrivee, date_depart, nb_pers) VALUES (?,?,?,?,?)";

        $res = mysqli_prepare($connect,$sql);
        
        $exe2 = mysqli_stmt_execute($res);

        mysqli_stmt_bind_param($res,"iissi", $num_cl, $num_ch, $enter, $exit,$pers);


        if($exe2){
            //header("location:add_booking.php");
            echo "BRAVO !!!";
        }else {
            echo "Echec !!!";
        }
    }
  }
}

?>
<h1 class="text-center">Nouvelle Réservation</h1> 

<div class="offest-3 col-md-6 text-center">
		<div class="offest-3 m-4 booking-form border p-3">

			<form action="" method="post">

        <h5 class="card-title">Détails nouveau client</h5> 

          <div class="form-row">
            <div class="form-group col-md">
              <input type="email" class="form-control" id="inputEmail4" placeholder="Email" name="email">
            </div>
          </div>

          <div class="form-group">
            <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St" name="adresse1">
          </div>

          <div class="form-group">
            <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor" name="adresse2">
          </div>

          <div class="form-row">

              <div class="form-group col-md-2">
                <input type="text" class="form-control" id="inputZip" placeholder="CP" name="cp">
              </div>

              <div class="form-group col-md-6">
                <input type="text" class="form-control" id="inputCity" placeholder="Ville" name="ville">
              </div>
              <div class="form-group col-md-4">
                <select id="inputState" class="form-control" name="pays">
                  <option selected>Pays</option>
                  <option value="France">France</option>
                  <option value="Espagne">Espagne</option>
                  <option value="Angleterre">Angleterre</option>
                  <option value="Italie">Italie</option>
                  <option value="Allemagne">Allemagne</option>
                </select>
              </div>
          </div>

          <div class="text-center">
            <button class="btn btn-primary btn-sm"><i class ="fa fa-plus-circle" name="submit"></i> Enregistrer</button>
          </div>
          
    <hr>

          <div class="form-group">
									<h5 class="form-label">Détails de la réservation</h5>
								</div>

                <div class="row text-center justify-content-center">
									
                  <div class="col-sm-6">
										<div class="form-group">
											<span class="form-label">Numéro client</span>
											<input class="form-control" type="text" name="num_cl" >
										</div>
									</div>

									<div class="col-sm-6">
										<div class="form-group">
											<span class="form-label">Numero chambre</span>
											<input class="form-control" type="text" name="num_ch" required>
										</div>
									</div>

								</div>
                
								<div class="row text-center justify-content-center">
									
                  <div class="col-sm-6">
										<div class="form-group">
											<span class="form-label">Date D'Entrée</span>
											<input class="form-control" type="date" name="entrer" required>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="form-group">
											<span class="form-label">Date De Sortie</span>
											<input class="form-control" type="date" name="sortir" required>
										</div>
									</div>

								</div>

				<div class="row text-center justify-content-center">

								<div class="col-sm-6">
                      <div class="form-group">
                          <span class="form-label">Nombres de personnes</span>
                          <select class="form-control" name="pers" required>
                            <option hidden>...</option>
                            <option value="1">1 personne</option>
                            <option value="2">2 personnes</option>
                            <option value="3">3 personnes</option>
                            <option value="4">4 personnes</option>
                            <option value="5">5 personnes</option>
                            <option value="6">6 personnes</option>
                          </select>
                          <span class="select-arrow"></span>
                      </div>
								</div>
				</div>


        <div class="form-group">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="gridCheck">
              <label class="form-check-label text-justify" for="gridCheck">
                <small><em>J'ai lu et accepte les conditions générales d'utilisation et la politique de confidentialité de Valentines.com ?</em></small>
              </label>
            </div>
        </div>

        <div class="text-right">
            <button class="btn btn-warning btn-lg"><i class ="fa fa-plus-circle" name="submit"></i> Réservez</button>
          </div>



        </form>
	  </div>
</div>

<?php require_once("../partials/footer.php");?>