<?php
$connect = mysqli_connect("127.0.0.1", "root","","auberge" );
require_once("../partials/header.php");
?>

<?php

if(isset($_POST["submit"]) && !empty($_POST["categorie"]) && !empty($_POST["capacite"]) && !empty($_POST["nb_lit"]) && !empty($_POST["prix"]) && !empty($_POST["etat"]) && !empty($_POST["descr"])){
    $categorie = trim(addslashes(htmlentities($_POST["categorie"])));
    $pers = (int)trim(addslashes(htmlentities($_POST["capacite"])));
    $lit = (int)trim(addslashes(htmlentities($_POST["nb_lit"])));
    $prix = (int)trim(addslashes(htmlentities($_POST["prix"])));
    $etat = trim(addslashes(htmlentities($_POST["etat"])));
    $descr = trim(addslashes(htmlentities($_POST["descr"])));

    $img = $_FILES["img"]["name"];
    $dest = "../images/";
    move_uploaded_file($_FILES["img"]["tmp_name"], $dest.$_FILES["img"]["name"]);

    foreach($_POST["confort"] as $cle => $valeurs){
        $confort[] = $cle;
        $equipment = implode(", ",$confort); 
    }

    if($connect){
        $sql = "INSERT INTO chambre(num_chambre,prix, nb_lit, nb_pers, confort, image, description, categorie, etat) VALUES(?,?,?,?,?,?,?,?,?)";


        $res = mysqli_prepare($connect,$sql);
        
        $bind = mysqli_stmt_bind_param($res,"iiiisssss",$num_ch,$prix, $lit, $pers, $equipment, $img, $descr, $categorie, $etat);

        $exe2 = mysqli_stmt_execute($res);

        if($exe2){
            header("location:tab_chambres.php");

        }else {
            echo "Echec !!!";
        }

    }
}

?>
<h1>Nouvelle Chambre</h1>
<form class="m-4" action="" method="post" enctype ="multipart/form-data">
  <div class="row">
    <div class="col">
    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="categorie">
        <option hidden>Catégorie</option>
        <option value="Suites classiques">Suites classiques</option>
        <option value="Chambre simple">Chambre simple</option>
        <option value="Chambre double">Chambre double</option>
      </select>
    </div>
    <div class="col">
    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="capacite">
        <option hidden>Capacité max.</option>
        <option value="1">1 personne</option>
        <option value="2">2 personnes</option>
        <option value="3">3 personnes</option>
        <option value="4">4 personnes</option>
        <option value="5">5 personnes</option>
        <option value="6">6 personnes</option>
      </select>
    </div>
    <div class="col">
    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="nb_lit">
        <option hidden>Nombre de lits</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
      </select>
    </div>
    <div class="col">
      <input type="text" class="form-control" placeholder="Prix" name="prix">
    </div>
  </div>
  <div class="row">
    <div class="custom-file col-9 m-3">
        <input type="file" class="custom-file-input" id="validatedCustomFile" name="img">
        <label class="custom-file-label" for="validatedCustomFile">Choisir une image...</label>
    </div>
    <div class="col-2 m-3 ">
    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="etat">
        <option hidden>Etat</option>
        <option value="Disponible">Disponible</option>
        <option value="Reservée">Réservée</option>
      </select>
    </div>
  </div>
<div class="row m-2">
<div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="confort[Wifi]">
        <label class="form-check-label">Wifi</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="confort[TV-LCD]">
        <label class="form-check-label">TV-LCD</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="confort[Coffre-fort]">
        <label class="form-check-label">Coffre-fort</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="confort[Cafétière]">
        <label class="form-check-label" for="inlineCheckbox1">Cafétière</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="confort[Snacks]">
        <label class="form-check-label" for="inlineCheckbox2">Snacks</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="confort[Baignoire]">
        <label class="form-check-label" for="inlineCheckbox1">Baignoire</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="confort[Jacuzzi]">
        <label class="form-check-label" for="inlineCheckbox1">Jacuzzi</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="confort[Double-douche]">
        <label class="form-check-label" for="inlineCheckbox2">Double-douche</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="confort[Non-fumeur]">
        <label class="form-check-label" for="inlineCheckbox2">Non-fumeur</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="confort[Climatisation]">
        <label class="form-check-label" for="inlineCheckbox2">Climatisation</label>
    </div>
</div>

<div class="row justify-content-center">
    <div class="form-group w-100 m-3">
    <textarea class="form-control" id="exampleFormControlTextarea3" rows="15" placeholder="Description..." name="descr"></textarea>
    </div>
</div>

<div class="text-right m-4">
    <button href="add.php" class="btn btn-warning" type="submit" name="submit"><i class ="fa fa-plus-circle"></i> Ajouter</button>
</div>

</form>

<?php require_once("../partials/footer.php");?>