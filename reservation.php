<?php
require_once("partials/header.php");
$connect = mysqli_connect("127.0.0.1", "root","","auberge" );

if(isset($_GET["num_ch"])){
    $ch_pro = (int)$_GET["num_ch"];
    if($connect){
        $sql = "SELECT * FROM reservation WHERE num_chambre =".$ch_pro;
        $res = mysqli_prepare($connect, $sql);
        $sql_exe = mysqli_stmt_execute($res);
        $bind = mysqli_stmt_bind_result($res,$num_cl, $num_ch, $enter, $exit,$pers);
        var_dump($enter);
    }
}

?>

<h1>Récapitulatif de réservation</h1>
<div class="row">
  <div class="col-sm-6 ">
    <div class="card m-4">
      <div class="card-body">
        <h5 class="card-title">Détails de votre réservation</h5>
        <table class="table-bordered ">
            <tr>
                <th>Chambre n°</th>
                <td><?=$pers;?></td>
            </tr>
            <tr>
                <th>Date d'arrivée</th>
                <td><?=$enter;?></td>
            </tr>
            <tr>
                <th>Date de départ</th>
                <td><?=$exit;?></td>
            </tr>
            <tr>
                <th>Nombre de personnes</th>
                <td><?=$pers;?></td>
            </tr>
        </table>
        <hr>
        <h5 class="card-title">Détails Client</h5> 

        <form>
          <div class="form-row">
            <div class="form-group col-md">
              <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
            </div>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
          </div>
          <div class="form-row">

          <div class="form-group col-md-2">
              <input type="text" class="form-control" id="inputZip" placeholder="CP">
            </div>

            <div class="form-group col-md-6">
              <input type="text" class="form-control" id="inputCity" placeholder="Ville">
            </div>
            <div class="form-group col-md-4">
              <select id="inputState" class="form-control">
                <option selected>Pays</option>
                <option>France</option>
                <option>Espagne</option>
                <option>Angleterre</option>
                <option>Italie</option>
                <option>Allemagne</option>
              </select>
            </div>

          </div>
          <div class="form-group">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="gridCheck">
              <label class="form-check-label" for="gridCheck">
                <small><em>J'ai lu et accepte les conditions générales d'utilisation et la politique de confidentialité de Valentines.com ? </em></small>
              </label>
            </div>
          </div>
          <div class="text-right">
            <a class="btn btn-warning btn-lg" href="admin/booking.php"><i class ="fa fa-plus-circle"></i> Reservez</a>
          </div>
</form>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card m-4">
      <div class="card-body">
        <h5 class="card-title">Facture</h5>
        <table class="table-bordered m-3">
            <tr>
                <th>Nombre de nuit</th>
                <td>10</td>
            </tr>
            <tr>
                <th>T.V.A</th>
                <td>10</td>
            </tr>
            <tr>
                <th>Taxe de Séjour </th>
                <td>10</td>
            </tr>
            <tr>
                <th>TOTAL</th>
                <td>10</td>
            </tr>
        </table>
        <a href="#" class="btn btn-warning btn-md disabled" role="button" aria-disabled="true">Confirmer la réservation</a>
        <a href="accueil.php" class="btn btn-danger">Annuler</a>
      </div>
    </div>
  </div>
</div>

<?php require_once("partials/footer.php");?>