<?php
require_once("partials/header.php");
$connect = mysqli_connect("127.0.0.1", "root","","auberge" );

if(isset($_GET["num_ch"])){
  if($connect){
	$ch_pro = (int)$_GET["num_ch"];

      $sql = "SELECT * FROM chambre WHERE num_chambre =".$ch_pro;
	  $res = mysqli_prepare($connect,$sql);
	  
	  $sql_exe = mysqli_stmt_execute($res);
      mysqli_stmt_bind_result($res, $num_ch, $prix, $nb_lit, $nb_pers,$confort,$img,$descr,$categorie,$etat);
	  $tab_ch = mysqli_stmt_fetch($res);
	}
}


?>
<div class="row">
 
</div>
<div id="booking" class="section">
		<div class="section-center">
			<div class="container">
				<div class="row">

					<div class="m-3 col-md-6 col-md-push-5 border">
              <div class="col-md-10 blogShort">
                     <h3><?=$categorie;?></h3>
                     <img  src="images/<?=$img;?>" alt="ch_img" class="pull-left mr-4" width="250px">

                     <div class="ml-4 price"><h1><?=$prix;?> &euro;</h1><small> Chambre n° : <?=$num_ch;?> </small></div>
                
                    <div class="stats">
                    <span><i class="fa fa-bed"></i> <?=$nb_lit;?> Lits</span>
                </div><br>

				<button class="btn btn-success"><i class="fa fa-check-circle"></i> <?=$etat?></button>
				
				<br><br><br>
                        <em>Equipement : <?=$confort?></em>
                     <article><br><p class = "text-justify"><?=$descr;?></p></article>
                 </div>
          </div>
          
					<div class="col-md-5 col-md-pull-7">
						<div class="m-4 booking-form border p-3">
							<form action="admin/add_booking.php" method="post">
								<div class="form-group">
									<h3 class="form-label">Votre Réservation</h3>
								</div>
								<div class="row text-center justify-content-center">
									<div class="col-sm-6">
										<div class="form-group">
											<span class="form-label">Date D'Entrée</span>
											<input class="form-control" type="date" name="entrer" value="<?=$enter?>" required>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="form-group">
											<span class="form-label">Date De Sortie</span>
											<input class="form-control" type="date" name="sortir" value="<?=$sortir?>" required>
										</div>
									</div>

								</div>
								<div class="row text-center justify-content-center">
									<div class="col-sm-6">
										<div class="form-group">
											<span class="form-label">Nombres de personnes</span>
											<select class="form-control" name="pers" required>
												<option hidden>...</option>
												<option><?=$enter?></option>
												<option value="1">1 personne</option>
												<option value="2">2 personnes</option>
												<option value="3">3 personnes</option>
												<option value="4">4 personnes</option>
												<option value="5">5 personnes</option>
												<option value="6">6 personnes</option>>	
											</select>
											<span class="select-arrow"></span>
										</div>
									</div>
								</div>
								<div class="text-center">
    								<button class="btn btn-warning" name="submit" type="submit"><i class ="fa fa-plus-circle"></i>Reservez</button>
							</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php require_once("partials/footer.php");?>