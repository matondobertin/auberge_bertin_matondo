<!-- Footer -->
<footer class="page-footer font-small blue pt-4 border bg-dark text-white">
  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">
    <!-- Grid row -->
    <div class="row">
      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">
        <!-- Content -->
        <h5 class="text-uppercase">LE VALENTINE'S</h5>
        <p>
        31, Rue Stéphane Berne, Geneviève, 51069, France<br> 
        <i class="fa fa-phone"></i> : +33 (0) 22 552 90 95<br> 
        <i class="fa fa-envelope"></i> : info@thevalentines.com
        </p>
      </div>

      <!-- Grid column -->
      <hr class="clearfix w-100 d-md-none pb-3">
    
      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">
        <!-- Links -->
        <h5 class="text-uppercase">LE VALENTINE'S</h5>
        <ul class="list-unstyled">
          <li>
            <a href="http://localhost/php/procedurale/auberge/accueil.php">Accueil</a>
          </li>
          <li>
            <a href="http://localhost/php/procedurale/auberge/presentation.php">Présentation</a>
          </li>
          <li>
            <a href="http://localhost/php/procedurale/auberge/reservation.php">Réservations</a>
          </li>
          <li>
            <a href="http://localhost/php/procedurale/auberge/contact.php">Contact</a>
          </li>
        </ul>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">
        <!-- Links -->
        <h5 class="text-uppercase">Newsletters</h5>
        <div class="span12">
    		<div class="thumbnail center well well-small">                       
                <form action="" method="post">
                    <div class="input-prepend"><span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" id="" name="" placeholder="your@email.com">
                    </div>
                    <br />
                    <input type="submit" value="Subscribe Now!" class="btn btn-large" />
              </form>
            </div>    
        </div> 
      </div>
      <!-- Grid column -->
    </div>
    <!-- Grid row -->
  </div>
  <!-- Footer Links -->
 
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Copyright : Website by 
    <a href="#">watchdogs-squad.com</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->

    </div>    
</body>



</html>