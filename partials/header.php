<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script>
        $(function(){
            $('#datetimepicker1').datetimepicker();
        });
    </script>

    <title>Cigogne</title>
</head>
<body>
    <div class="container border">
    <nav class="navbar navbar-expand-lg navbar-white bg-dark">
        <a class="navbar-brand" href="#"><img src="images/ibis_logo.png" alt="logo" width="75"></a>
        <button class="navbar-toggler text-primary border" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        Menu<span class="navbar-toggler-icon"><i class="fa fa-bars text-white"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="http://localhost/php/procedurale/auberge/accueil.php">Accueil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost/php/procedurale/auberge/presentation.php">Présentation</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost/php/procedurale/auberge/contact.php">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost/php/procedurale/auberge/admin/tab_chambres.php">Les Chambres</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost/php/procedurale/auberge/admin/booking.php">Les Réservations</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item avatar dropdown">
                    <a class="nav-link dropdown-toggle text-white mr-auto" id="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user text-white fa-2x"></i>
                        <!--<img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-2.jpg" class="rounded-circle z-depth-0" alt="avatar image">-->
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary" aria-labelledby="navbarDropdownMenuLink-55">
                        <a class="dropdown-item" href="http://localhost/php/procedurale/auberge/login.php">Connexion</a>
                        <a class="dropdown-item" href="#">Vos réservations</a>
                        <a class="dropdown-item" href="http://localhost/php/procedurale/auberge/admin/logout.php">Déconnexion</a>
                    </div>
                </li>
            </ul>
  </div>
</nav>